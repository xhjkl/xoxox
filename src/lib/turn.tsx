export type Turn = { side: ('x' | 'o'), position: [number, number] }
