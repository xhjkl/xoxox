/// Return an item from `items` picked at random.
export default (items) => items[Math.floor(Math.random() * items.length)]
