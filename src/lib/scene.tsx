//
//  Core game logic.
//
import foldField from '../lib/fold-field'
import chooseRandom from '../lib/choose-random'
import { Turn } from '../lib/turn'

export default class Scene {

  turns: Turn[] = []

  getField () {
    return foldField(this.turns)
  }

  reset () {
    this.turns = []
  }

  addTurn (turn: Turn) {
    this.turns.push(turn)
  }

  takeTurnAsBot () {
    // A bit of an oversimplification of `xkcd.com/832/`.

    const field = this.getField()
    const fieldIsCompletelyFilled = field
      .map((row) => row.every((cell) => cell != null))
      .reduce((x, y) => x && y)

    if (fieldIsCompletelyFilled) {
      return null
    }

    const [xs, os] = [].concat(...field)
      .reduce(([xs, os], cell) => (cell === 'x' ? [xs + 1, os] : cell === 'o' ? [xs, os + 1] : [xs, os]), [0, 0])
    const botIsEligibleForTakingTurn = (xs === (os + 1))
    if (!botIsEligibleForTakingTurn) {
      return null
    }

    while (true) {
      let y = chooseRandom([1, 2, 3])
      let x = chooseRandom([1, 2, 3])
      if (field[y - 1][x - 1] == null) {
        const turn = { side: 'o', position: [y, x] } as Turn
        this.addTurn(turn)
        return turn
      }
    }
  }
}
