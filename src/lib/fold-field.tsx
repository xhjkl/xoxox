//
//  Algorithmic definitions for the play field bookkeeping.
//

/// Reconstruct the field state from separate turn events.
export default (turns) => {
  let field = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ]

  turns.forEach(({ side, position: [y, x] }) => {
    field[y - 1][x - 1] = side
  })

  return field
}
