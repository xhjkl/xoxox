//
//  Database operations.
//
import Scene from '../lib/scene'
import { MongoClient } from 'mongodb'

/// Callback invoked after the database is checked.
///
///  error -- Error on failure or null on success.
///
type Responder = (error?: Error) => void

/// So far this is a singleton,
/// but it could be easily separated into a per-client state.
const scene = new Scene()

let db = null

/// Asynchronously tell if the database is operational.
export const check = (work: Responder) => {
  MongoClient.connect(process.env.DATABASE_URL, { useNewUrlParser: true }, (error, client) => {
    if (error != null) {
      work(error)
      return
    }
    db = client.db('xo').collection('xo')
    db.findOne({}).then((result) => {
      if (result == null) {
        db.insertOne({}).then(() => {
          work(null)
        })
      } else {
        const foundTurns = result.turns
        scene.turns = foundTurns || scene.turns
        work(null)
      }
    })
  })
}

export const getTurns = () => {
  return scene.turns
}

export const addTurn = ({ side, position }) => {
  if (db) {
    db.updateOne(
      {},
      {
        $set: { turns: [ ...scene.turns, { side, position } ] },
      },
      { upsert: true },
    )
  }

  scene.addTurn({ side, position })
}

export const takeTurnAsBot = () => {
  const turn = scene.takeTurnAsBot()

  if (db) {
    db.updateOne(
      {},
      {
        $set: { turns: [ ...scene.turns, turn ] },
      },
      { upsert: true },
    )
  }

  return turn
}

export const reset = () => {
  if (db) {
    db.updateOne(
      {},
      {
        $set: { turns: [] },
      },
      { upsert: true },
    )
  }

  scene.reset()
}
