//
//  Separated logic for Websocket message handling
//
import * as socketIO from 'socket.io'

import * as db from './db'

// Make Websocket work
//
//  server -- `http.Server` instance
//
export default ({ server }) => {
  const socket = socketIO(server)
  socket.on('connect', (client) => {
    client.on('event', (event) => {
      const { eventKind } = event

      switch (eventKind) {
        case 'reloadTurns':
          const turns = db.getTurns()
          client.emit('event', { type: 'overrideTurns', turns })
          break
        case 'userDidTakeTurn':
          db.addTurn(event)
          break
        case 'botShouldTakeTurn':
          const turn = db.takeTurnAsBot()
          if (turn == null) {
            break
          }
          const { side, position } = turn
          client.emit('event', { type: 'didTakeTurn', side, position })
          break
        case 'userDidRequestReset':
          db.reset()
          break
      }
    })
  })
}
