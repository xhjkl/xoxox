//
//  Entry point for the server side
//
import * as path from 'path'
import * as http from 'http'

// We can not use static imports in the main module,
// because we want to populate the env before loading the deps.
const dotenv = require('dotenv')
const { parsed: parsedDotenv, error: dotenvError } = dotenv.config()
if (dotenvError != null) {
  console.warn('check dotenv:', dotenvError)
}

const express = require('express')

const db = require('./db')
const attachWebsocketEndpoint = require('./websocket').default

const serveComponent = require('./serve-component').default
const Root = require('../client/container/Root').default
const Frame = require('../client/container/Frame').default

const StaticDir = 'public'
const BuildDir = 'build/public'

let app = express()
app.disable('x-powered-by')

const sourceTreeRoot = path.resolve(__dirname, '..', '..')
app.use(express.static(path.resolve(sourceTreeRoot, StaticDir)))
app.use(express.static(path.resolve(sourceTreeRoot, BuildDir)))

app.get('/', (req, res) => {
  serveComponent(
    res,
    Frame, { title: 'XO Tournament' },
    Root, { },
  )
})

app.use((req, res) => {
  res.status(404).sendFile(
    'notfound.html',
    { root: path.resolve(__dirname, '..', '..', 'src', 'client') },
  )
})

let server = http.createServer(app)
attachWebsocketEndpoint({ server })

// Go
db.check((error) => {
  if (error != null) {
    console.error(error)
    process.exit(3)
  }

  server.listen(process.env.PORT || 31337, () => {
    console.log(server.address())
  })
})
