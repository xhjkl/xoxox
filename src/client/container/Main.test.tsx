/** @jest-environment jsdom
 */
import * as React from 'react'
import * as renderer from 'react-test-renderer'
import { Main } from './Main'

it('should render error page', () => {
  let compo = renderer.create(
    <Main error='Something went wrong.' />,
  )

  expect(compo.toJSON()).toMatchSnapshot()
})

it('should render start page', () => {
  let compo = renderer.create(
    <Main />,
  )

  expect(compo.toJSON()).toMatchSnapshot()
})
