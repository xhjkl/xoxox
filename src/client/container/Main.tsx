import * as React from 'react'
import { connect } from 'react-redux'
import foldField from '../../lib/fold-field'
import { didBeginGame, didTryToTakeTurn, didTryToReset } from '../store/action'
import ErrorScreen from '../view/ErrorScreen'
import CenteredBox from '../view/CenteredBox'
import Button from '../view/Button'
import Playfield from '../view/Playfield'
import StatusLine from '../view/StatusLine'

const mapStateToProps = ({ error, inLobby, victoriousSide, turns }) => ({ error, inLobby, victoriousSide, turns })
const mapDispatchToProps = (dispatch) => ({
  didTryToTakeTurn: (y, x) => { dispatch(didTryToTakeTurn({ side: 'x', position: [y, x] })) },
  didTryToReset: () => { dispatch(didTryToReset()) },
  didBeginGame: () => { dispatch(didBeginGame()) },
})

type Props = {
  error?: string,
  inLobby?: boolean,
  victoriousSide?: null | 'x' | 'o',
  turns?: { side: 'x' | 'o', position: [number, number] }[],
  didTryToTakeTurn?: (y, x) => void,
  didTryToReset?: () => void,
  didBeginGame?: () => void,
}

export class Main extends React.Component<Props> {

  render () {
    const {
      error,
      inLobby,
      victoriousSide,
      turns,
      didTryToTakeTurn,
      didTryToReset,
    } = this.props

    if (error != null) {
      return (
        <ErrorScreen>
          { error }
        </ErrorScreen>
      )
    }

    if (inLobby) {
      return (
        <CenteredBox>
          <Button onClick={ this.props.didBeginGame }>PLAY &raquo;</Button>
        </CenteredBox>
      )
    }
    return (
      <CenteredBox>
        <Playfield field={ foldField(turns) } onClick={ (y, x) => { didTryToTakeTurn(y, x) } }/>
        <StatusLine>
          {
            victoriousSide === 'x'
              ? 'You win.'
              : victoriousSide === 'o'
                ? 'Opponent wins.'
                : ''
          }
        </StatusLine>
        { victoriousSide != null && <Button onClick={ didTryToReset }>AGAIN</Button> }
      </CenteredBox>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main)
