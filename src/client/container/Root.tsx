//
// Root component
//
import * as React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { injectGlobal } from 'emotion'
import createHistory from '../store/createHistory'
import configureStore, { runSagaMiddleware } from '../store/configureStore'
import Main from '../container/Main'

injectGlobal`
  html, body, #root {
    font: 100% sans-serif;
    background: hsl(0, 0%, 97%);
    display: flex;
    flex-direction: column;
    min-width: 100vw;
    min-height: 100vh;
    margin: 0;
    padding: 0;
  }
`

type Props = {
  loggedIn: boolean,
}

type State = {
  loggedIn: boolean,
}

export default class Root extends React.Component<Props, State> {

  store = null
  history = null

  constructor (props) {
    super(props)
    this.history = createHistory()
    this.store = configureStore({ history: this.history })
  }

  componentDidMount () {
    runSagaMiddleware()
  }

  render () {
    return (
      <Provider store={this.store}>
        <ConnectedRouter history={this.history}>
          <Main />
        </ConnectedRouter>
      </Provider>
    )
  }
}
