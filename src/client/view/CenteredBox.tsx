import styled from 'react-emotion'

export default styled('div')(() => ({
  flex: '0 1',
  margin: 'auto',
}))
