import * as React from 'react'
import styled from 'react-emotion'

const Table = styled('table')((props) => ({
  width: '300px',
  height: '300px',
  borderRadius: '4px',
  background: '#fff',
  boxShadow: '1px 1px 4px hsla(0, 0%, 0%, 0.1)',
  cursor: 'pointer',
  userSelect: 'none',
  borderCollapse: 'collapse',
  boxSizing: 'border-box',
} as any))

const Row = styled('tr')((props) => ({
  borderTop: '4px solid hsla(0, 0%, 96%, 1)',
  '&:nth-of-type(1)': {
    borderTop: '4px solid hsla(0, 0%, 96%, 0)',
  },
} as any))

const Cell = styled('td')(({ x, o }: {x?: boolean, o?: boolean}) => ({
  border: '4px solid hsla(0, 0%, 96%, 1)',
  borderTop: 'none',
  borderBottom: 'none',
  background: x ? 'url(/x.svg)' : (o ? 'url(/o.svg)' : 'transparent'),
  width: '96px',
  height: '96px',
  '&:nth-of-type(1)': {
    borderLeft: '4px solid hsla(0, 0%, 96%, 0)',
  },
  '&:nth-of-type(3)': {
    borderRight: '4px solid hsla(0, 0%, 96%, 0)',
  },
} as any))

export default ({ field, onClick }: { field: ('x' | 'o' | null)[][], onClick?: (y: number, x: number) => void }) => (
  <Table>
    <tbody>
      { [1, 2, 3].map((rowN) => (
        <Row key={ `${rowN}` }>
          { [1, 2, 3].map((colN) => (
            <Cell
              key={ `${rowN}${colN}` }
              x={ field[rowN - 1][colN - 1] === 'x' }
              o={ field[rowN - 1][colN - 1] === 'o' }
              onClick={ (onClick != null) ? onClick.bind(this, rowN, colN) : null }
            />
          )) }
        </Row>
      )) }
    </tbody>
  </Table>
)
