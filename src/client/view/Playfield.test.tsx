/** @jest-environment jsdom
 */
import * as React from 'react'
import * as renderer from 'react-test-renderer'
import Playfield from './Playfield'

it('should render empty play field', () => {
  let compo = renderer.create(
    <Playfield field={ [[], [], []] }/>,
  )

  expect(compo.toJSON()).toMatchSnapshot()
})

it('should render occupied play field', () => {
  let compo = renderer.create(
    <Playfield field={ [['o', 'x'], [], []] } />,
  )

  expect(compo.toJSON()).toMatchSnapshot()
})
