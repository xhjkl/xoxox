import * as React from 'react'
import styled from 'react-emotion'

export default styled('a')((props) => ({
  fontSize: '3rem',
  fontWeight: '800',
  border: '0.3px solid hsla(0, 0%, 0%, 0.1)',
  padding: '1.2rem',
  borderRadius: '4px',
  background: '#fff',
  boxShadow: '1px 1px 4px hsla(0, 0%, 0%, 0.1)',
  cursor: 'pointer',
  userSelect: 'none',
} as any))
