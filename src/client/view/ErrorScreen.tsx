import * as React from 'react'
import CenteredBox from '../view/CenteredBox'

export default ({ children }) => (
  <CenteredBox>
    <h2>{ ...children }</h2>
  </CenteredBox>
)
