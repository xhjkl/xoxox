//
//  Redux-related definitions.
//

/// Enumeration of all available action kinds.
export type ActionKind =
  'error'
  | 'didBeginGame'
  | 'didTryToTakeTurn'
  | 'didTakeTurn'
  | 'overrideTurns'
  | 'didWin'
  | 'didTryToReset'

/// Action could be an arbitrary object with a `type` field.
export type Action = { type: ActionKind, [name: string]: any }

export const setError = (message): Action => ({ type: 'error', message })
export const clearError = (): Action => ({ type: 'error', message: null })
export const didBeginGame = (): Action => ({ type: 'didBeginGame' })
export const didTryToTakeTurn = ({
  side,
  position,
}: {
  side: 'x' | 'o',
  position: [number, number],
}): Action => ({
  type: 'didTryToTakeTurn',
  side,
  position,
})
export const didTakeTurn = ({
  side,
  position,
}: {
  side: 'x' | 'o',
  position: [number, number],
}): Action => ({
  type: 'didTakeTurn',
  side,
  position,
})
export const didWin = ({ side }: { side: 'x' | 'o' }): Action => ({ type: 'didWin', side })
export const didTryToReset = (): Action => ({ type: 'didTryToReset' })
