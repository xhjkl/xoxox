//
//  Root reducer definitions.
//
import { combineReducers } from 'redux'
import { Action } from './action'

export default combineReducers({
  error (state = null, action: Action) {
    switch (action.type) {
      case 'error':
        return action.message
    }

    return state
  },

  inLobby (state = true, action: Action) {
    switch (action.type) {
      case 'didBeginGame':
        return false
    }

    return state
  },

  sideToTakeTurn (state = 'x', action: Action) {
    switch (action.type) {
      case 'didTakeTurn':
        return ['x', 'o'][+(state === 'x')]
    }

    return state
  },

  victoriousSide (state = null, action: Action) {
    switch (action.type) {
      case 'didBeginGame':
        return null
      case 'didWin':
        return action.side
    }

    return state
  },

  turns (state = [], action: Action) {
    switch (action.type) {
      case 'didBeginGame':
        return []
      case 'didTakeTurn':
        const { side, position } = action
        return [...state, { side, position }]
      case 'overrideTurns':
        return action.turns
    }

    return state
  },
})
