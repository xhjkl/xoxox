//
//  Universal adapter for `create*History` methods.
//
import createBrowserHistory from 'history/createBrowserHistory'
import createMemoryHistory from 'history/createMemoryHistory'

/// Use in-memory History for server; History API for client.
const createHistory = (
  (typeof self === typeof void 0)
    ? createMemoryHistory
    : createBrowserHistory
)

export default createHistory
