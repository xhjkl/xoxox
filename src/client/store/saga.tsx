//
//  Root saga definitions.
//
import { eventChannel } from 'redux-saga'
import { call, select, take, put, all, takeEvery } from 'redux-saga/effects'
import foldField from '../../lib/fold-field'
import { didTakeTurn, didWin } from './action'

const subscribeWithSocket = () => eventChannel((emit) => {
  const { socket } = self
  socket.on('event', emit)
  return () => {
    socket.off('event')
  }
})

const communicateIn = function* () {
  const eventChannel = subscribeWithSocket()
  while (true) {
    const event = yield take(eventChannel)
    yield put(event)
  }
}

const communicateOut = function* () {
  const { socket } = self
  while (true) {
    const action = yield take('shouldSendMessage')
    socket.emit('event', action.payload)
  }
}

const communicate = function* () {
  const { socket } = self
  if (socket == null) {
    return
  }

  yield all([
    communicateIn(),
    communicateOut(),
  ])
}

const reloadTurns = function* (action) {
  yield put({ type: 'shouldSendMessage', payload: { eventKind: 'reloadTurns' } })
}

const recordTurn = function* (action) {
  const { turns, sideToTakeTurn } = yield select(
    ({ turns, sideToTakeTurn }) => ({ turns, sideToTakeTurn }),
  )
  const field = foldField(turns)
  const { side, position } = action
  const [y, x] = position
  if (field[y - 1][x - 1]) {
    // Cell already filled.
    return
  }
  if (sideToTakeTurn !== side) {
    // Not our turn.
    return
  }
  yield put(didTakeTurn({ side, position }))
}

const broadcastTurn = function* (action) {
  const { side, position } = action
  if (side !== 'x') {
    return
  }
  yield put({ type: 'shouldSendMessage', payload: { eventKind: 'userDidTakeTurn', side, position } })
}

const checkVictory = function* (action) {
  const { turns } = yield select(({ turns }) => ({ turns }))
  const field = foldField(turns)
  if (field[0][0] != null && field[0][0] === field[1][1] && field[1][1] === field[2][2]) {
    yield put(didWin({ side: field[0][0] }))
    return
  }
  if (field[0][2] != null && field[0][2] === field[1][1] && field[1][1] === field[2][0]) {
    yield put(didWin({ side: field[0][2] }))
    return
  }
  for (let row = 0; row < 3; row += 1) {
    if (field[row][0] != null && field[row][0] === field[row][1] && field[row][1] === field[row][2]) {
      yield put(didWin({ side: field[row][0] }))
      return
    }
  }
  const transposed = field[0].map((col, i) => field.map(row => row[i]))
  for (let col = 0; col < 3; col += 1) {
    if (transposed[col][0] != null && transposed[col][0] === transposed[col][1] && transposed[col][1] === transposed[col][2]) {
      yield put(didWin({ side: transposed[col][0] }))
      return
    }
  }
}

const requestOpponentTurn = function* (action) {
  const { sideToTakeTurn } = yield select(({ sideToTakeTurn }) => ({ sideToTakeTurn }))
  if (sideToTakeTurn !== 'o') {
    // Human player's turn.
    return
  }

  yield put({ type: 'shouldSendMessage', payload: { eventKind: 'botShouldTakeTurn' } })
}

const reset = function* (action) {
  yield put({ type: 'shouldSendMessage', payload: { eventKind: 'userDidRequestReset' } })
  yield put({ type: 'didBeginGame' })
}

const rootSaga = function* () {
  yield all([
    communicate(),
    takeEvery('didBeginGame', reloadTurns),
    takeEvery('didTakeTurn', broadcastTurn),
    takeEvery('didTakeTurn', checkVictory),
    takeEvery('didTakeTurn', requestOpponentTurn),
    takeEvery('didTryToTakeTurn', recordTurn),
    takeEvery('didTryToReset', reset),
  ])
}

export default rootSaga
