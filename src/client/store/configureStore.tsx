//
//  Topmost store configuration.
//
import { createStore, applyMiddleware } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './reducer'
import rootSaga from './saga'

export const sagaMiddleware = createSagaMiddleware()

export const runSagaMiddleware = () => {
  sagaMiddleware.run(rootSaga as any)
}

export default ({ history, ...initialState }: any) => createStore(
  connectRouter(history)(rootReducer),
  initialState,
  applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware,
  ),
)
